package com.cmb;

import com.cryptape.cita.crypto.sm2.SM2;

import com.cryptape.cita.crypto.sm2.SM3;
import com.cryptape.cita.protobuf.ConvertStrByte;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

/**
 * @author CMB
 */
public class Test {

    public static void main(String args[]) throws Exception {

        // --------------------- SM2生成国密算法公私钥对测试 -------------------
        KeyInfo keyInfo = SM2Algo.newKeyPair();
        // 生成的公钥自行妥善保存
        System.out.println("SM2私钥：" + keyInfo.privateKey);
        // 生成的私钥自行妥善保存，不应向任何第三方展示
        System.out.println("SM2公钥：" + keyInfo.publicKey);
        // 生成的地址提供给招商银行，在开放许可链管理平台准入
        System.out.println("SM2地址：" + keyInfo.address);

        System.out.println("****************************************");

        // --------------------- SM2非对称加解密测试 -------------------
        String data = "ASDF1234我爱北京天安门”%……&*";
        byte[] sm2CipherText = SM2Algo.encrypt(data);
        System.out.println("SM2加密：" + ByteUtils.toHexString(sm2CipherText));
        String sm2PlainText = SM2Algo.decrypt(sm2CipherText);
        System.out.println("SM2解密：" + sm2PlainText);

        System.out.println("****************************************");

        // --------------------- SM2签名验签测试 -------------------
        SM2.Signature sign = SM2Algo.sign(data);
        System.out.println("SM2签名：" + sign.getSign());
        boolean result = SM2Algo.verify(data, sign);
        System.out.println("SM2验签：" + result);

        System.out.println("****************************************");

        // --------------------- SM3计算合同文件HASH值测试 -------------------
        // f66f821c5c703105d4a593ac75d84b704804514f6b20260dcc0730b14eca7064
        String hash1 = SM3Algo.hash("E:\\Projects\\family-trust-gm\\测试文件1.pdf");
        // 906c2ee53587d2f26065a4b1ca3a2f86aa95232babb6ed1fc7eefd54d0bb30cb
        String hash2 = SM3Algo.hash("E:\\Projects\\family-trust-gm\\测试文件2.txt");
        // 0806e48f920457aa009900b6222d9324220d173d6e5ceca2606e0c975c9ce6d3
        String hash3 = SM3Algo.hash("E:\\Projects\\family-trust-gm\\测试文件3.jpg");

        // 以下三个hash请请根据测试标准结果校验
        System.out.println("SM3哈希：" + hash1);
        System.out.println("SM3哈希：" + hash2);
        System.out.println("SM3哈希：" + hash3);
        // 55e12e91650d2fec56ec74e1d3e4ddbfce2ef3a65890c2a19ecf88a307e76a23
        System.out.println("SM3哈希：" + SM3Algo.hashOfPlainText("test"));

        System.out.println("****************************************");

        // --------------------- SM4对称加解密测试 -------------------
        byte[] sm4Key = SM4Algo.generateKey(128);
        System.out.println("SM4密钥：" + ByteUtils.toHexString(sm4Key));
        byte[] sm4CipherText = SM4Algo.encryptEcbPadding(sm4Key, data.getBytes());
        System.out.println("SM4加密：" + ByteUtils.toHexString(sm4CipherText));
        byte[] sm4PlainText = SM4Algo.decryptEcbPadding(sm4Key, sm4CipherText);
        System.out.println("SM4解密：" + new String(sm4PlainText));


        // --------------------- 新的SM2签名验签测试 -------------------
        //请设置为正确的私钥值
        String privateKey = "7b455774f60ef5090efeba0d8c342d53881316d70373d92d8ce8608766XXXXXX";
        String newData = "ASDF1234我爱北京天安门”%……&*";
        SM2.Signature newSign = SM2Algo.sign(newData.getBytes(), privateKey);
        System.out.println("SM2签名：" + newSign.getSign());
        boolean newResult = SM2Algo.verify(newData.getBytes(), newSign, privateKey);
        System.out.println("SM2验签：" + newResult);

        System.out.println("****************************************");


        // --------------------- 新的SM2签名验签测试（存证服务场景） -------------------
        //存证服务构造交易结果(oriTxData)
        String text = "1220613535316362643266656237346663386164383265396361393638663132656618c4830220f5fcf5092a2e7b2264617465223a22323032322d30322d31382031343a35303a3530222c226e616d65223a22e6b58be8af95227d3220000000000000000000000000000000000000000000000000000000000000000040024a14ffffffffffffffffffffffffffffffffff01000052200000000000000000000000000000000000000000000000000000000000000002";
        System.out.println("text(存证服务oriTxData): "+text);
        //计算摘要
        byte[] hash = SM3.hash(ConvertStrByte.hexStringToBytes(text));
        //计算签名
        SM2.Signature signature = SM2Algo.sign(hash, privateKey);
        //拼装签名值
        byte[] sigData = SM2.getSignature(signature, new SM2().fromPrivateKey(privateKey).getPublicKey());
        //将最终结果转化为16进制结果(sigTxData)
        String sigResult = ConvertStrByte.bytesToHexString(sigData);
        System.out.println("sig result(存证服务sigTxData)：" + sigResult);

        System.out.println("****************************************");
    }
}
