package com.cmb;

/**
 * @author CMB
 */
public class KeyInfo {
    public String publicKey;
    public String privateKey;
    public String address;
}
