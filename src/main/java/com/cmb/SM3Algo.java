package com.cmb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

/**
 * @author CMB
 */
public class SM3Algo {

    /**
     * 国密SM3获取文件HASH
     *
     * @param filePath 待加密文件路径
     */
    public static String hash(String filePath) {
        // 修改此处文件路径，完成本地测试验证
        try (FileInputStream fis = new FileInputStream(new File(filePath))) {
            byte[] buffer = new byte[4096];
            SM3Digest sm3 = new SM3Digest();

            // 读取文件，并将字节更新到SM3中
            int readNum = 0;
            while ((readNum = fis.read(buffer)) != -1) {
                sm3.update(buffer, 0, readNum);
            }
            byte[] hash = new byte[sm3.getDigestSize()];

            // 计算HASH值
            sm3.doFinal(hash, 0);

            return ByteUtils.toHexString(hash);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 国密SM3获取文件HASH
     *
     * @param text 待加密文件路径
     */
    public static String hashOfPlainText(String text) {
        // 修改此处文件路径，完成本地测试验证

        SM3Digest sm3 = new SM3Digest();

        // 读取文件，并将字节更新到SM3中
        byte[] textBytes = text.getBytes();
        sm3.update(textBytes, 0, textBytes.length);

        byte[] hash = new byte[sm3.getDigestSize()];

        // 计算HASH值
        sm3.doFinal(hash, 0);

        return ByteUtils.toHexString(hash);
    }
}
